"""a module to house basic condor logic
"""
__author__ = "Reed Essick (reed.essick@gmail.com)"

#-------------------------------------------------

import os
import getpass
from distutils.spawn import find_executable

#-------------------------------------------------

SUB = '''\
universe = %(universe)s
executable = %(exe)s
arguments = %(arguments)s
output = %(logdir)s/$(JOB).out
error = %(logdir)s/$(JOB).err
log = %(logdir)s/$(JOB).log
getenv = True
request_memory = %(request_memory)d
request_cpus = %(request_cpus)d
request_disk = %(request_disk)d
accounting_group = %(group)s
accounting_group_user = %(user)s
queue 1'''

#output = %(logdir)s/%(name)s-$(JOB).out
#error = %(logdir)s/%(name)s-$(JOB).err
#log = %(logdir)s/%(name)s-$(JOB).log

JOB = '''\
JOB   %(name)s %(sub)s
RETRY %(name)s %(retry)d\n'''
VARS = 'VARS  %(name)s %(vars)s\n' ### should be concatenated at the end of JOB as needed

PARENT_CHILD = 'PARENT %(parent)s CHILD %(child)s\n'

#-------------------------------------------------

DEFAULT_UNIVERSE = 'vanilla'
DEFAULT_REQUEST_MEMORY = 500000 # 500 MB
DEFAULT_REQUEST_CPUS = 1
DEFAULT_REQUEST_DISK = 100000 # 100 MB

def sub(
        path,
        exe,
        args,
        logdir,
        accounting_group, 
        universe=DEFAULT_UNIVERSE,
        request_memory=DEFAULT_REQUEST_MEMORY,
        request_cpus=DEFAULT_REQUEST_CPUS,
        request_disk=DEFAULT_REQUEST_DISK,
        accounting_group_user=None,
        verbose=False,
    ):
    """write SUB file
    """
    if verbose:
        print('writing %s SUB: %s'%(exe, path))

    if accounting_group_user is None:
        accounting_group_user = getpass.getuser()

    with open(path, 'w') as obj:
        obj.write(SUB%{
            'universe':universe,
            'exe':find_executable(exe),
            'arguments':' '.join(args),
            'logdir':logdir,
            'group':accounting_group,
            'user':accounting_group_user,
            'request_memory':request_memory,
            'request_cpus':request_cpus,
            'request_disk':request_disk,
        })

    return path

#-----------

def shell(
        path,
        exe,
        args,
        verbose=False,
    ):
    """write SUB file
    """
    if verbose:
        print('writing %s : %s'%(exe, path))

    with open(path, 'w') as obj:
        obj.write('#!/bin/bash\n')
        obj.write('\n' + find_executable(exe))
        for arg in args:
            obj.write('\\\n    '+arg)

    os.system('chmod +x '+path) # make shell script executable

    return path

#------------------------

SAMPLE_NAME = 'mcvt-sample-%d'
DEFAULT_SAMPLE_UNIVERSE = 'vanilla'
DEFAULT_SAMPLE_REQUEST_CPUS = 1

def sample_memory_requirement(num_samples, save_vector_attributes=False):
    """these should be conservative scalings with the size of the sample set
    """
    return 100000 + (1700 if save_vector_attributes else 4) * num_samples

def sample_disk_requirement(num_samples, save_vector_attributes=False):
    """these should be conservative scalings with the size of the sample set
    """
    return int((2000 if save_vector_attributes else 0.5) * num_samples)

def sample_sub(
        config,
        num_samples,
        logdir,
        accounting_group,
        store_logprob=False,
        skip_factored_logprob=False,
        save_vector_attributes=False, 
        gps_range=None,
        seed=None,
        backend=None,
        verbose=None,
        Verbose=None,
        debug=None,
        accounting_group_user=None,
        universe=DEFAULT_SAMPLE_UNIVERSE,
        request_memory=None, ### scale this automatically with the number of samples requested
        request_cpus=DEFAULT_SAMPLE_REQUEST_CPUS,
        request_disk=None, ### scale this automatically with the number of samples requested
    ):
    """write a SUB file for mcvt-sample jobs
    """
    path = os.path.join(logdir, 'mcvt-sample.sub')

    # set up arguments
    sample_args = [
        config,
        str(num_samples),
        '--output-path', '$(SAMPLE_OUTPUT)',
    ]

    if gps_range:
        sample_args += ['--gps-range'] + [str(_) for _ in gps_range]

    if seed is not None:
        sample_args += ['--seed', '$(SEED)']

    if backend is not None:
        sample_args += ['--backend', backend]

    if store_logprob:
        sample_args += ['--store-logprob']

    if skip_factored_logprob:
        sample_args += ['--skip-factored-logprob']

    if save_vector_attributes:
        sample_args += [ '--save-vector-attributes']

    if debug:
        sample_args += ['--debug']

    elif Verbose:
        sample_args += ['--Verbose']

    elif verbose:
        sample_args += ['--verbose']

    ### figure out how much disk to request
    ### these should be conservative scalings with the size of the sample set
    if request_memory is None:
        request_memory = sample_memory_requirement(num_samples, save_vector_attributes=save_vector_attributes)

    if request_disk is None:
        request_disk = sample_disk_requirement(num_samples, save_vector_attributes=save_vector_attributes)

    # write the file
    return sub(
        path,
        'mcvt-sample',
        sample_args,
        logdir,
        accounting_group,
        accounting_group_user=accounting_group_user,
        universe=universe,
        request_memory=request_memory,
        request_cpus=request_cpus,
        request_disk=request_disk,
        verbose=True,
    )

#-----------

def sample_shell(
        sample_name,
        output,
        config,
        num_samples_per_proc,
        logdir,
        store_logprob=False,
        skip_factored_logprob=False,
        save_vector_attributes=False,
        gps_range=None,
        seed=None,
        backend=None,
        verbose=None,
        Verbose=None,
        debug=None,
    ):
    """write a shell script for mcvt-sample
    """
    path = os.path.join(logdir, sample_name+'.sh')

    # set up arguments
    sample_args = [
        config,
        str(num_samples_per_proc),
        '--output-path', '$(SAMPLE_OUTPUT)',
    ]

    if gps_range:
        sample_args += ['--gps-range' + ' '.join(str(_) for _ in gps_range)]

    if seed is not None:
        sample_args += ['--seed', str(seed)]

    if backend is not None:
        sample_args += ['--backend', backend]

    if store_logprob:
        sample_args += ['--store-logprob']

    if skip_factored_logprob:
        sample_args += ['--skip-factored-logprob']

    if save_vector_attributes:
        sample_args += [ '--save-vector-attributes']

    if debug:
        sample_args += ['--debug']

    elif Verbose:
        sample_args += ['--Verbose']

    elif verbose:
        sample_args += ['--verbose']

    # write the file
    return shell(
        path,
        'mcvt-sample',
        sample_args,
        verbose=True,
    )

#------------------------

CONCATENATE_NAME = 'mcvt-concatenate-sample'
DEFAULT_CONCATENATE_UNIVERSE = 'vanilla'
DEFAULT_CONCATENATE_REQUEST_MEMORY = 500000 # 500 MB should be enough for most jobs
DEFAULT_CONCATENATE_REQUEST_CPUS = 1
DEFAULT_CONCATENATE_REQUEST_DISK = 500000 # 500 MB should be enough for most jobs

def concatenate_sub(
        logdir,
        accounting_group,
        verbose=False,
        Verbose=False,
        retain_individual_output=False,
        accounting_group_user=None,
        universe=DEFAULT_CONCATENATE_UNIVERSE,
        request_memory=DEFAULT_CONCATENATE_REQUEST_MEMORY,
        request_cpus=DEFAULT_CONCATENATE_REQUEST_CPUS,
        request_disk=DEFAULT_CONCATENATE_REQUEST_DISK,
    ):
    """write a SUB for mcvt-concatenate-sample jobs
    """
    path = os.path.join(logdir, 'mcvt-concatenate-sample.sub')

    # set up arguments
    concatenate_args = ['$(INDIVIDUAL_SAMPLE_OUTPUT)', '$(CONCATENATED_OUTPUT_PATH)']

    if Verbose:
        concatenate_args += ['--Verbose']

    elif verbose:
        concatenate_args += ['--verbose']

    if not retain_individual_output:
        concatenate_args += ['--remove-source']

    # figure out how much memory we want
    if request_memory is None:
        request_memory = 500000 ### 500 MB should be enough for most jobs 

    # write the file
    return sub(
        path,
        'mcvt-concatenate-sample',
        concatenate_args,
        logdir,
        accounting_group,
        accounting_group_user=accounting_group_user,
        universe=universe,
        request_memory=request_memory,
        request_cpus=request_cpus,
        request_disk=request_disk,
        verbose=True,
    )

#-----------

def concatenate_shell(
        individual_sample_output, # should be an iterable
        output,
        logdir,
        verbose=False,
        Verbose=False,
        retain_individual_output=False,
    ):
    """write a shell script for mcvt-concatenate-sample
    """
    path = os.path.join(logdir, CONCATENATE_NAME+'.sh')

    # set up arguments
    concatenate_args = list(individual_sample_output) + [output]

    if Verbose:
        concatenate_args += ['--Verbose']

    elif verbose:
        concatenate_args += ['--verbose']

    if not retain_individual_output:
        concatenate_args += ['--remove-source']

    # write the file
    return shell(
        path,
        'mcvt-concatenate-sample',
        concatenate_args,
        verbose=True,
    )

#------------------------

SUMMARIZE_NAME = 'mcvt-summarize-sample'
DEFAULT_SUMMARIZE_UNIVERSE = 'vanilla'
DEFAULT_SUMMARIZE_REQUEST_MEMORY = 500000 # 500 MB should be enough for most jobs
DEFAULT_SUMMARIZE_REQUEST_CPUS = 1
DEFAULT_SUMMARIZE_REQUEST_DISK = 500000 # 500 MB should be enough for most jobs

def summarize_memory_requirement(num_samples):
    return DEFAULT_SUMMARIZE_REQUEST_MEMORY ### FIXME: scale this with the sample size?

def summarize_disk_requirement(num_samples):
    return DEFAULT_SUMMARIZE_REQUEST_DISK ### FIXME: scale this with the sample size?

def summarize_sub(
        output_dir,
        logdir,
        accounting_group,
        snr_thresholds=[],
        columns=[],
        include_ndet=False,
        include_neff=False,
        verbose=False,
        Verbose=False,
        universe=DEFAULT_SUMMARIZE_UNIVERSE,
        accounting_group_user=None,
        request_memory=DEFAULT_SUMMARIZE_REQUEST_MEMORY,
        request_cpus=DEFAULT_SUMMARIZE_REQUEST_CPUS,
        request_disk=DEFAULT_SUMMARIZE_REQUEST_DISK,
    ):
    """write a SUB for mcvt-summarize-sample jobs
    """
    path = os.path.join(logdir, 'mcvt-summarize-sample.sub')

    # set up arguments
    summarize_args = [
        '$(CONCATENATED_OUTPUT_PATH)',
        '--output-dir', output_dir,
        '--plot-detected-distribution',
    ]

    for key, val in snr_thresholds:
        summarize_args += ['--snr-threshold', key, val]

    for column, label, log in columns:
        summarize_args += ['--column', column]
        if label is not None:
            summarize_args += ['--label', column, label]
        if log:
            summarize_args += ['--log', column]

    if include_ndet:
        summarize_args += ['--include-ndet']

    if include_neff:
        summarize_args += ['--include-neff']

    if Verbose:
        summarize_args += ['--Verbose']

    elif verbose:
        summarize_args += ['--verbose']

    # write the file
    return sub(
        path,
        'mcvt-summarize-sample',
        summarize_args,
        logdir,
        accounting_group,
        accounting_group_user=accounting_group_user,
        universe=universe,
        request_memory=request_memory,
        request_cpus=request_cpus,
        request_disk=request_disk,
        verbose=True,
    )

#-----------

def summarize_shell(
        output_path,
        logdir,
        snr_thresholds=[],
        columns=[],
        include_ndet=False,
        include_neff=False,
        verbose=False,
        Verbose=False,
    ):
    """write a shell script for mcvt-summarize-sample
    """

    path = os.path.join(logdir, SUMMARIZE_NAME+'.sh')

    # set up arguments
    summarize_args = [
        output_path,
        '--output-dir', os.path.dirname(output_dir),
        '--plot-detected-distribution',
    ]

    for key, val in snr_thresholds:
        summarize_args += ['--snr-threshold', key, val]

    for column, label, log in columns:
        summarize_args += ['--column', column]
        if label is not None:
            summarize_args += ['--label', column, label]
        if log:
            summarize_args += ['--log', column]

    if include_ndet:
        summarize_args += ['--include-ndet']

    if include_neff:
        summarize_args += ['--include-neff']

    if Verbose:
        summarize_args += ['--Verbose']

    elif verbose:
        summarize_args += ['--verbose']

    # write the file
    return shell(
        path,
        'mcvt-summarize-sample',
        summarize_args,
        verbose=True,
    )

#-------------------------------------------------

def sample_dag(
        config,
        num_samples_per_proc,
        num_proc,
        output_path,
        logdir,
        accounting_group,
        store_logprob=False,
        skip_factored_logprob=False,
        save_vector_attributes=False,
        gps_range=None,
        seed=None,
        backend=None,
        verbose=False,
        Verbose=False,
        debug=False,
        generate_shell_scripts=True,
        retain_individual_output=False,
        summarize_sample=False,
        snr_thresholds=[],
        columns=[],
        include_ndet=False,        
        include_neff=False,        
        retry=0,
        accounting_group_user=None,
        sample_universe=DEFAULT_SAMPLE_UNIVERSE,
        sample_request_cpus=DEFAULT_REQUEST_CPUS,
        sample_request_memory=None,
        sample_request_disk=None,
        concatenate_universe=DEFAULT_CONCATENATE_UNIVERSE,
        concatenate_request_cpus=DEFAULT_REQUEST_CPUS,
        concatenate_request_memory=None,
        concatenate_request_disk=None,
        summarize_universe=DEFAULT_SUMMARIZE_UNIVERSE,
        summarize_request_cpus=DEFAULT_REQUEST_CPUS,
        summarize_request_memory=None,
        summarize_request_disk=None,
    ):
    """write a DAG for the sample+concatenate workflow
    """
    ### handle logic to sample sequentially if samples are to be regularly spaced
    sequential_sampling = gps_range is not None
    if sequential_sampling:
        gps_start, gps_end = gps_range
        gps_dur = (gps_end - gps_start) / num_proc

        gps_range = ['$(GPS_START)', '$(GPS_END)'] ### pass to sample_sub so we can grab VARS from dag

    # write sample SUB

    if sample_request_memory is None:
        sample_request_memory = sample_memory_requirement(
            num_samples_per_proc,
            save_vector_attributes=save_vector_attributes,
        )

    if sample_request_disk is None:
        sample_request_disk = sample_disk_requirement(
            num_samples_per_proc,
            save_vector_attributes=save_vector_attributes,
        )

    sample = sample_sub(
        config,
        num_samples_per_proc,
        logdir,
        accounting_group,
        accounting_group_user=accounting_group_user,
        universe=sample_universe,
        request_memory=sample_request_memory,
        request_cpus=sample_request_cpus,
        request_disk=sample_request_disk,
        store_logprob=store_logprob,
        skip_factored_logprob=skip_factored_logprob,
        save_vector_attributes=save_vector_attributes,
        gps_range=gps_range,
        seed=seed,
        backend=backend,
        verbose=verbose,
        Verbose=Verbose,
        debug=debug,
    )

    # write concatenate SUB

    if concatenate_request_memory is None:
        concatenate_request_memory = sample_memory_requirement(
            num_samples_per_proc*num_proc,
            save_vector_attributes=save_vector_attributes,
        )

    if concatenate_request_disk is None:
        concatenate_request_disk = sample_disk_requirement(
            num_samples_per_proc*num_proc,
            save_vector_attributes=save_vector_attributes,
        )

    concatenate = concatenate_sub(
        logdir,
        accounting_group,
        accounting_group_user=accounting_group_user,
        universe=concatenate_universe,
        request_memory=concatenate_request_memory,
        request_cpus=concatenate_request_cpus,
        request_disk=concatenate_request_disk,
        verbose=verbose,
        Verbose=Verbose,
        retain_individual_output=retain_individual_output,
    )

    if summarize_sample: # write summarize SUB

        if summarize_request_memory is None:
            summarize_request_memory = summarize_memory_requirement(num_samples_per_proc*num_proc)

        if summarize_request_disk is None:
            summarize_request_disk = summarize_disk_requirement(num_samples_per_proc*num_proc)

        summarize = summarize_sub(
            os.path.dirname(output_path),
            logdir,
            accounting_group,
            accounting_group_user=accounting_group_user,
            universe=summarize_universe,
            request_memory=summarize_request_memory,
            request_cpus=summarize_request_cpus,
            request_disk=summarize_request_disk,
            snr_thresholds=snr_thresholds,
            columns=columns,
            include_ndet=include_ndet,
            include_neff=include_neff,
            verbose=verbose,
            Verbose=Verbose,
        )

    # write DAG
    dag = os.path.join(logdir, 'mcvt-condor-sample.dag')
    print('writing mcvt-condor-sample DAG: '+dag)

    with open(dag, 'w') as obj:

        # write one job for each proc
        sample_output = []
 
        for proc in range(num_proc):
            sample_name = SAMPLE_NAME%proc

            if sequential_sampling: ### include VARS to define gps range for this job
                output = os.path.join(logdir, 'mcvt-sample-%d-%d.hdf'%(gps_start, gps_dur))
                extra_vars = ' GPS_START="%.6f" GPS_END="%.6f"'%(gps_start, gps_start+gps_dur)
                gps_start += gps_dur ### increment

            else:
                output = os.path.join(logdir, 'mcvt-sample-%d.hdf'%proc)
                extra_vars = ''

            if seed is not None:
                this_seed = seed + proc # unique for each proc
                extra_vars = extra_vars + ' SEED="%d"' % this_seed
            else:
                this_seed = None

            obj.write(JOB % {'name':sample_name, 'sub':sample, 'retry':retry})
            obj.write(VARS % {
                'name':sample_name,
                'vars': 'SAMPLE_OUTPUT="%s"' % output + extra_vars,
            })
            obj.write(PARENT_CHILD % {'parent':sample_name, 'child':CONCATENATE_NAME})

            if generate_shell_scripts:
                # write a shell script to accomplish the same job
                sample_shell(
                    sample_name,
                    output,
                    config,
                    num_samples_per_proc,
                    logdir,
                    store_logprob=store_logprob,
                    skip_factored_logprob=skip_factored_logprob,
                    save_vector_attributes=save_vector_attributes,
                    gps_range=gps_range,
                    seed=this_seed,
                    backend=backend,
                    verbose=verbose,
                    Verbose=Verbose,
                    debug=debug,
                )

            # add this output to the list
            sample_output.append(output)

        ### then write the concatenate job
        obj.write(JOB % {'name':CONCATENATE_NAME, 'sub':concatenate, 'retry':retry})
        obj.write(VARS % {
            'name':CONCATENATE_NAME,
            'vars':'INDIVIDUAL_SAMPLE_OUTPUT="%s" CONCATENATED_OUTPUT_PATH="%s"' % \
                (' '.join(sample_output), output_path)
        })

        if generate_shell_scripts:
            concatenate_shell(
                sample_output,
                output_path,
                logdir,
                verbose=verbose,
                Verbose=Verbose,
                retain_individual_output=retain_individual_output,
            )

        if summarize_sample: ### finally, write sample job
            obj.write(JOB % {'name':SUMMARIZE_NAME, 'sub':summarize, 'retry':retry})
            obj.write(VARS % {'name':SUMMARIZE_NAME, 'vars':'CONCATENATED_OUTPUT_PATH="%s"'%output_path})
            obj.write(PARENT_CHILD % {'parent':CONCATENATE_NAME, 'child':SUMMARIZE_NAME})

            if generate_shell_scripts:
                summarize_shell(
                    output_path,
                    logdir,
                    snr_thresholds=snr_thresholds,
                    columns=columns,
                    include_ndet=include_ndet,
                    include_neff=include_neff,
                    verbose=verbose,
                    Verbose=Verbose,
                )

    ### return path to DAG
    return dag
