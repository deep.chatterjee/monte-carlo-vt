"""a small library to handle the task of writing custom file formats to disk
"""
__author__ = "Reed Essick (reed.essick@gmail.com)"

#-------------------------------------------------

import os

from collections import defaultdict

import h5py
import numpy as np

# non-standard libraries
from gwdistributions.backends import names

try:
    from LDAStools import frameCPP as frcpp # used to write frames
except ImportError:
    frcpp = None

#-------------------------------------------------

DEFAULT_SERIES_SUFFIX = "gwf"

GWF_SERIES_SUFFIXES = ["gwf"]
HDF_SERIES_SUFFIXES = ["hdf", "hdf5", "h5"]
KNOWN_SERIES_SUFFIXES = GWF_SERIES_SUFFIXES + HDF_SERIES_SUFFIXES

#-------------------------------------------------

def float2sec_ns(gps):
    gps_int = int(gps)
    gps_ns = int(1e9 * (gps - gps_int)) # NOTE, this could introduce truncation error at nsec level
    return gps_int, gps_ns

def sec_ns2float(gps_sec, gps_ns):
    return gps_sec + 1e-9*gps_ns

#-------------------------------------------------

def write_series(path, *args, **kwargs):
    """write time series data to disk
    data should be a structured array (keys are channel names)
    gps_start and dt define the properties of the time span
    """
    suffix = path.split('.')[-1]
    if suffix in GWF_SERIES_SUFFIXES:
        return write_series_gwf(path, *args, **kwargs)

    elif suffix in HDF_SERIES_SUFFIXES:
        return write_series_hdf(path, *args, **kwargs)

    else:
        raise ValueError('suffix=%s for %s not understood! must be one of: %s' % \
            (suffix, path, ', '.join(KNOWN_SERIES_SUFFIXES)))

def load_series(path, *args, **kwargs):
    """read time series data from disk
    """
    suffix = path.split('.')[-1]
    if suffix in GWF_SERIES_SUFFIXES:
        return load_series_gwf(path, *args, **kwargs)

    elif suffix in HDF_SERIES_SUFFIXES:
        return load_series_hdf(path, *args, **kwargs)

    else:
        raise ValueError('suffix=%s for %s not understood! must be one of: %s' % \
            (suffix, path, ', '.join(KNOWN_SERIES_SUFFIXES)))

#-------------------------------------------------

def write_series_gwf(path, data, gps_start, dt, frame_dur=None, verbose=False):
    """write time series data into a GWF frame
    data should be a structured array (keys are channel names)
    gps_start and dt define the properties of the time span
    """

    if frcpp is None:
        raise ImportError('could not import LDAStools.frameCPP')

    #---

    num_samples = len(data)
    duration = num_samples * dt

    if frame_dur is None:
        frame_dur = duration

    else:
        assert (duration % frame_dur) == 0, \
        'duration=%d must be an integer multiple of frame_dur=%d' \
            % (duration, frame_dur)

    num_frames = int(duration // frame_dur) ### number of frames per file
    samples_per_frame = int(frame_dur / dt) ### number of samples per frame

    #---

    if verbose:
        print('writing series data (%d samples) as %d frames (%d samples/frame) to : %s' % (duration/dt, num_frames, samples_per_frame, path))

    stream = frcpp.OFrameFStream(path)

    for ind in range(num_frames):

        start = gps_start + ind*frame_dur

        samples_start = ind*samples_per_frame # use this to grab the appropriate sample set for this frame
        samples_end = samples_start + samples_per_frame

        if verbose:
            print('    writing frame %d : %d -> %d (%d : %d)' % (ind, start, start+frame_dur, samples_start, samples_end))

        # make the frame
        frame = frcpp.FrameH()

        # set basic parameters of the frame
        frame.SetName('%d-%d'%(start, frame_dur)) # NOTE: SetName could be fragile
        frame.SetGTime(frcpp.GPSTime(*float2sec_ns(start)))
        frame.SetDt(frame_dur)

        # iterate over data, adding each dtype as
        for channel in data.dtype.names:

            if verbose:
                print('        writing : '+channel)

            # allocate the vector
            frvect = frcpp.FrVect(
                channel,
                frcpp.FrVect.FR_VECT_8R,
                1,                      # number of dimensions
                frcpp.Dimension(        # the dimension object
                    samples_per_frame,  # number of elements
                    dt,                 # spacing
                    's',                # units
                    0,                  # start time offset relative to the header (set above)
                ),
                '',  # dimensionless unit for strain
            )

            # actually populate the object
            frvect.GetDataArray()[:] = data[channel][samples_start:samples_end]

            # add that vector to the ProcData
            frdata = frcpp.FrProcData(
                channel,
                '',                                 # comment
                frcpp.FrProcData.TIME_SERIES,       # ID as time-series
                frcpp.FrProcData.UNKNOWN_SUB_TYPE,  # empty sub-type (fseries)
                0,                                  # offset of first sample relative to frame start
                frame_dur,                          # duration of data
                0.0,                                # heterodyne frequency
                0.0,                                # phase of heterodyne
                0.0,                                # frequency range
                0.0,                                # resolution bandwidth
            )

            # add the vector to this data
            frdata.AppendData(frvect)

            # add ProcData to FrameH
            frame.AppendFrProcData(frdata)

        # write
        frame.Write(stream)

#------------------------

def load_series_gwf(path, verbose=False):
    """load series from a GWF file
    """
    if verbose:
        print('loading series data from : '+path)

    stream = frcpp.IFrameFStream(path)

    frame = stream.ReadNextFrame()  # assume at least one frame

    gps_start = sec_ns2float(*frame.GetGTime()) # NOTE, the following is too restrictive in general
    dt = None                                   # but it should work for the types of frames we write
    num_samples = None                          # in this library

    #---

    toc = stream.GetTOC()

    channels_fr_data = [
        (toc.GetProc(), stream.ReadFrProcData),
        (toc.GetADC(), stream.ReadFrAdcData),
        (toc.GetSim(), stream.ReadFrSimData),
    ]

    #---

    seriesdict = defaultdict(list)
    num_frames = 0
    while frame:

        if verbose:
            print('    loading frame %d' % num_frames)

        # extract data
        for channels, fr_data in channels_fr_data:
            for channel in channels:
                if verbose:
                    print('        found : '+channel)

                # magic number: one frame per stream...
                data = fr_data(num_frames, channel)
                dim = data.data[0].GetDim(0)

                # magic number: one FrVect wrapper per channel
                data = data.data[0].GetDataArray()
                if dt is None:
                    dt = dim.dx
                    num_samples = len(data)
                else:
                    assert dt == dim.dx, 'dim.dx conflicts between channels!'
                    assert num_samples == len(data), 'len(data) conflicts between channels!'

                seriesdict[channel].append(np.array(data[:], dtype=float))

        num_frames += 1

        try:
            frame = stream.ReadNextFrame()  # assume at least one frame

        except IndexError: # signals that there are no more frames
            break

    assert len(seriesdict), 'no channels found!'

    # map dictionary to a structured array
    series = np.empty(num_samples*num_frames, dtype=[(channel, float) for channel in seriesdict.keys()])
    for channel, data in seriesdict.items():
        series[channel] = np.concatenate(tuple(data))

    # return
    return series, gps_start, dt

#-------------------------------------------------

def write_series_hdf(path, data, gps_start, dt, verbose=False, **kwargs):
    """write time series data into a HDF file
    data should be a structured array (keys are channel names)
    gps_start and dt define the properties of the time span
    """

    num_samples = len(data)
    duration = num_samples * dt

    #---

    if verbose:
        print('writing series data (%d samples) to : %s' % (duration/dt, path))

    with h5py.File(path, 'w') as obj:

        obj.attrs.create('gps_start', gps_start)
        obj.attrs.create('deltat', dt)

        # store data as a single dataset (structured array) rather than multiple dataset
        if verbose:
            for channel in data.dtype.names:
                print('        writing : '+channel)
        obj.create_dataset('series', data=data)

#------------------------

def load_series_hdf(path, verbose=False):
    """load series data from an HDF file
    """
    if verbose:
        print('loading series data from : '+path)

    with h5py.File(path, 'r') as obj:
        gps_start = obj.attrs['gps_start']
        dt = obj.attrs['deltat']

        series = obj['series'][:]

    # map dictionary to a structured array
    if verbose:
        for channel in series.dtype.names:
            print('    found : '+channel)

    return series, gps_start, dt

#-------------------------------------------------

# maps between LSC-HDF names to gw-distribution names
CBC_WAVEFORM_PARAMS_GROUP_NAME = 'cbc_waveform_params'

CBC_WAVEFORM_PARAMS = {
    'mass1_det' : 'mass1_detector',
    'mass2_det' : 'mass2_detector',
    'spin1x' : 'spin1x',
    'spin1y' : 'spin1y',
    'spin1z' : 'spin1z',
    'spin2x' : 'spin2x',
    'spin2y' : 'spin2y',
    'spin2z' : 'spin2z',
    'd_lum' : 'luminosity_distance',
    'ra' : 'right_ascension',
    'dec' : 'declination',
    'inclination' : 'inclination',
    'polarization' : 'polarization',
    'coa_phase' : 'coalescence_phase',
    'eccentricity' : 'eccentricity',
    't_co_gps' : 'time_geocenter',
    'cbc_model' : 'approximant',
    'f22_start' : 'waveform_flow',
    'f22_ref_spin' : 'waveform_fref',
    'longitude_of_ascending_nodes' : 'longAscNodes',
    'mean_periastron_advance' : 'meanPerAno',
}

# fields that we fill in with default values
CBC_SIM_DATA_GROUP_NAME = 'cbc_sim_data'
CBC_SIM_DATA = {
#    'delta_time_geocenter' : 't_co_gps_add',
#    'sim_id' : 'cbc_sim_id',
}

def write_lschdf(path, events, verbose=False, **meta):
    """write tabular data (numpy structured array) to LSC HDF file format
    https://git.ligo.org/waveforms/o4-injection-file/-/blob/main/FILES.md
    """
    num_events = len(events)

    if verbose:
        print('writing %d events to %s' % (num_events, path))

    with h5py.File(path, 'w') as obj:

        # create attrs
        for key, val in [('file_format', b'lvk_o4_injection')] + list(meta.items()):
            if verbose:
                print('creating attr : %s = %s' % (key, val))
            obj.attrs.create(key, val)

        # store parameters
        for group_name, params in [
                (CBC_WAVEFORM_PARAMS_GROUP_NAME, CBC_WAVEFORM_PARAMS),
                (CBC_SIM_DATA_GROUP_NAME, CBC_SIM_DATA),
            ]:
            group = obj.create_group(group_name)
            for key, val in params.items():
                if verbose:
                    print('creating dataset : %s : %s' % (group_name, key))

                if names.name(val) in events.dtype.names:
                    data = events[names.name(val)]

                else:
                    if verbose:
                        print('    dataset does not exist! setting to zeros')
                    data = np.zeros(num_events, dtype=float)

                group.create_dataset(key, data=np.zeros(num_events, dtype=float))
