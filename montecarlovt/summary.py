"""diagnostic plots and sensitivity estimates to summarize the behavior of individual populations
"""
__author__ = "Reed Essick (reed.essick@gmail.com)"

#-------------------------------------------------

import os
import warnings

import numpy as np
#import h5py

from scipy.stats import gamma

import matplotlib
matplotlib.use("Agg")
from matplotlib import pyplot as plt
plt.rcParams['font.family'] = 'serif'
#plt.rcParams['text.usetex'] = True

### non-standard libraries
from gwdistributions.backends import names

#-------------------------------------------------

### general utilities

def errors(n, b, N):
    """return error estimates needed to put shading on histograms
    """
    n = np.clip(np.array(n, dtype=float), 0, 1)
    s = (n*(1-n)/N)**0.5
    x = []
    y = []
    dy = []
    for n, s, b, B in zip(n, s, b[:-1], b[1:]):
        x += [b,B]
        y += [n]*2
        dy += [s]*2
    y = np.array(y)
    dy = np.array(dy)
    return x, y, dy

#-------------------------------------------------

### basic counting experiments

def counting_statistics(events, total_generated, columns=[]):
    """compute the number of detected events, the acceptance fraction, and basic properties of the specified columns
    """
    n = len(events)
    a = float(n)/total_generated
    da = (a*(1-a)/total_generated)**0.5
    return n, a, da, dict((col, (np.min(events[col]), np.max(events[col]))) for col in columns)

def report_counting_statistics(num_found, accept_frac, daccept_frac, columns):
    print("""\
    number found        = %d
    acceptance fraction = %.3e +/- %.3e"""%(num_found, accept_frac, daccept_frac))
    for key, (m, M) in columns.items():
        print("""\
    min observed %s = %.3f
    max observed %s = %.3f"""%(key, m, key, M))

#-------------------------------------------------

def pdet(weights, Ninj):
    Nfnd = len(weights)

    ### compute point estimate
    _pdet = np.sum(weights)/Ninj

    ### estimate uncertainty
    sample_variance = np.sum(((Nfnd/Ninj)*weights - _pdet)**2) / (Nfnd-1)
    sigma_pdet = (sample_variance / Nfnd)**0.5 # convert to variance on pdet

    return _pdet, sigma_pdet

#------------------------

### sensitive volume

def sensitive_volume(weights, Ninj):
    """expect weights to carry units of volume
    """
    return pdet(weights, Ninj)

def report_sensitive_volume(title, vol, sigma_vol, unit='Mpc^3'):
    print("""\
    %s
        <V> = %.6e +/- %.6e [%s]"""%(title, vol, sigma_vol, unit))

#------------------------

### effective number of samples

def effective_sample_size(weights, Ninj):
    _pdet, _sigma_pdet = pdet(weights, Ninj)

    ### return
    return _pdet, _sigma_pdet, _pdet_sigma2neff(_pdet, _sigma_pdet), len(weights)

def _pdet_sigma2neff(pdet, sigma_pdet):
    return (pdet / sigma_pdet)**2

def report_effective_sample_size(title, pdet, sigma_pdet, neff, nfnd):
    print("""\
    %s
        P(det) = %.6e +/- %.6e
        Neff = %10.3f
        Nfnd = %6d
        efficiency = (Neff/Nfnd) = %.6e"""%(title, pdet, sigma_pdet, neff, nfnd, neff/nfnd))

#------------------------

'''
### estimates of sensitivity to specific populations
# Rate estimation with specific populations is based on : https://dcc.ligo.org/LIGO-T2000100

def rate_estimate(
        Nobs,
        Tdet_sec,
        events,
        total_generated,
        generator,
        cosmology,
        z_ref=0.0,
        num_grid_points=1000,
        num_monte_carlo=10000,
    ):
    """estimate the rate of events
    """
    # reference for cosmology as well
    max_z = np.max(events[names.name('redshift')])

    # compute scaling functions

    raise NotImplementedError('need to remove time distribution!')

    prob = generator.prob(events)
    pdrw = np.exp(events[names.name('logprob')]) ### may be fragile...

    # compute the rate at a reference redshift
    redshift_dist = generator.variate2distribution(names.name('redshift'))
    fact = redshift_dist.prob(z_ref) / (cosmology.z2dVcdz(z_ref)/(1.+z_ref))  ### this will be in CGS

    # compute detected fraction
    det_frac = np.sum( prob / pdrw ) / total_generated
    var = (np.sum( (prob / pdrw)**2 ) / total_generated - det_frac**2) / total_generated
    sig = var**0.5

    # marginalize over monte carlo uncertainty to obtain p(rate)

    ### upper and lower bounds from Poisson counting uncertainty
    ### should give us reasonable ranges for rate posterior
    eps_mle = Nobs
    q = 0.00001

    ### expected number is Gamma distibuted: p(eps) ~ eps**Nobs * exp(-eps)
    if eps_mle == 0: ### one-sided CR
        low = 0
        hgh = gamma.isf(q, Nobs+1)

    else: ### two-sided symmetric CR
        low = gamma.isf(1. - 0.5*q, Nobs+1)
        hgh = gamma.isf(0.5*q, Nobs+1)

    ### set up the grid over reasonable ranges of possible rates
    smallest_scale = fact / ((det_frac + 3*sig) * Tdet_sec) ### 3-sigma above mean
    lowest = low * smallest_scale

    if det_frac > 3*sig:
        largest_scale = fact / ((det_frac - 3*sig) * Tdet_sec)
        highest = hgh * largest_scale
    else:
        warnings.warn('large Monte Carlo uncertainty in detected fraction! Rate estimate may not be precise')
        highest = (hgh * fact / (det_frac * Tdet_sec)) * 1000 ### just an approximation that should hopefully be ok...

    grid = np.logspace(np.log10(lowest), np.log10(highest), num_grid_points)
    pRate = np.zeros(num_grid_points, dtype=float)

    ### sample from monte carlo uncertainty on frac to numerically marginalize
    for frac in np.random.normal(det_frac, scale=sig, size=num_monte_carlo):
        scale = fact / (frac*Tdet_sec)
        pRate += gamma.pdf(grid/scale, Nobs+1) / scale ### include jacobian from count to rate
    pRate /= num_monte_carlo ### normalize (take the average over MC realizations)

    # return
    return det_frac, sig, grid, pRate

#------------------------

# convert from 1/(cm**3 * s) --> 1/(Gpc**3 * yr)
RATE_UNITS = (3.086e+27)**3 * (365*86400)

def report_rate_estimate(Nobs, det_frac, sig, grid, pRate, z_ref=0.0):
    mle_estimate = grid[np.argmax(pRate)]
    if Nobs == 0: ### note, the following could be improved!
        upper_bound = np.interp(0.90, np.cumsum(pRate), grid)
        lower_bound = 0.

    else:
        upper_bound = np.interp(0.95, np.cumsum(pRate), grid)
        lower_bound = np.interp(0.05, np.cumsum(pRate), grid)

    neff = (det_frac/sig)**2

    print("""\
    detected fraction = %.4e +/- %.5e --> Neff = %.1f
    Rate(z=%.3e) = %.3e^{+%.3e}_{-%.3e} [1/(cm**3 * s)]
              = %.3e^{+%.3e}_{-%.3e} [1/(Gpc**3 * yr)]""" % \
            (det_frac, sig, neff, \
            z_ref, mle_estimate, upper_bound, lower_bound, \
            mle_estimate*RATE_UNITS, upper_bound*RATE_UNITS, lower_bound*RATE_UNITS)
        )

#------------------------

def write_rate_estimate(path, Nobs, det_frac, sig, grid, pRate, z_ref=0.0):
    with h5py.File(path, 'w') as obj:
        for key, val in [
                ('Nobs', Nobs),
                ('detected_fraction', det_frac),
                ('sigma_detected_fraction', sig),
                ('reference_redshift', z_ref),
            ]:
            obj.attrs.create(key, val)

        obj.create_dataset(
            'rate_distribution',
            data=np.array(np.transpose([grid, pRate]), dtype=[('rate', float), ('p(rate)', float)]),
        )

#------------------------

def plot_rate_estimate(grid, pRate, z_ref=0.0, label=None, fig=None):
    """generate a plot of the uncertainty on the rate estimate
    """
    if fig is None:
        fig = plt.figure(figsize=(5, 4))
        ax = fig.add_axes([0.10, 0.12, 0.87, 0.70])
        xmin = +np.infty
        xmax = -np.infty
    else:
        ax = fig.gca()
        xmin, xmax = ax.get_xlim()

    # plot
    ax.plot(grid*RATE_UNITS, grid*pRate, label=label)

    # update x limits
    xmin = min(xmin, np.min(grid[pRate>0])*RATE_UNITS)
    xmax = max(xmax, np.max(grid[pRate>0])*RATE_UNITS)

    # decorate plot
    ax.set_xlabel('$\mathcal{R}(z=%.3f)\ [1/\mathrm{Gpc}^3 \mathrm{yr}]$'%z_ref)
    ax.set_ylabel(r'$\mathcal{R} \cdot p(\mathcal{R})\ [\mathrm{Gpc}^3 \mathrm{yr}]$')
    plt.setp(ax.get_yticklabels(), visible=False)

    ax.grid(True, which='both')

    ax.set_xscale('log')
    ax.set_xlim(xmin=xmin, xmax=xmax)

#    ax.set_ylim(ymin=0.0)

    # return
    return fig
'''

#-------------------------------------------------

### detected distributions

def samples2range(samples, log=False):
    m = np.min(samples)
    M = np.max(samples)

    if log:
        assert np.all(samples > 0), 'can only logscale positive-definite variables!'
        if m == M:
            dm = 0.05
        else:
            dm = (M/m)**0.05
        m /= dm
        M *= dm

    else:
        if m == M: # no dynamic range
            if m == 0: ### scaling won't work
                dm = 1.0
            else:
                dm = 0.05*m
        else:
            dm = (M-m)*0.05
        m -= dm
        M += dm

    return m, M

def stacked_histogram(samples, column, label=None, log=False, fig=None):
    """make cumulative and differential histograms of the detected distribution
    """
    if fig is None:
        fig = plt.figure(figsize=(4,6))
        ax1 = fig.add_subplot(2,1,1)
        ax2 = fig.add_subplot(2,1,2)
        xmin = +np.infty
        xmax = -np.infty

    else:
        ax1, ax2 = fig.axes
        xmin, xmax = ax1.get_xlim()

    # set up basic parameters

    Ndet = len(samples)
    kwargs = dict(histtype='step')

    m, M = samples2range(samples, log=log)
    xmin = min(xmin, m) # protect limits from previous calls
    xmax = max(xmax, M)

    if log:
        bins = np.logspace(np.log10(m), np.log10(M), max(10, int(Ndet**0.5)))
        cins = np.logspace(np.log10(m), np.log10(M), 10*Ndet)
    else:
        bins = np.linspace(m, M, max(10, int(Ndet**0.5)))
        cins = np.linspace(m, M, 10*Ndet)

    #--- cumulative histogram

    n, b, p = ax1.hist(
        samples,
        bins=cins,
        cumulative=-1,
        density=True,
        label='max{%s} = %.3f'%(column, np.max(samples)),
        **kwargs
    )
    color = p[0].get_edgecolor()

    # plot error estimates
    x, y, dy = errors(n, b, Ndet)
    ax1.fill_between(x, y-dy, y+dy, color=color[:3]+(0.5/3,), edgecolor='none')

    #--- differential histogram

    n, b, _ = ax2.hist(
        samples,
        bins=bins,
        color=color,
        density=True,
        label=label,
        **kwargs
    )

    # plot error estimates
    dx = b[1]-b[0]
    x, y, dy = errors(n*dx, b, Ndet)
    ax2.fill_between(x, (y-dy)/dx, (y+dy)/dx, color=color[:3]+(0.5/3,), edgecolor='none')

    #--- decorate
    if log:
        ax1.set_xscale('log')
        ax2.set_xscale('log')

    ax1.set_xlim(xmin=xmin, xmax=xmax)
    ax1.set_ylim(ymin=0.0, ymax=1.0)

    ax2.set_xlim(ax1.get_xlim())
#    ax2.set_ylim(ymin=0.0)

    plt.setp(ax1.get_xticklabels(), visible=False)
    ax2.set_xlabel(column)

    ax1.set_ylabel('P(%s|det)'%column)
    plt.setp(ax1.get_yticklabels(), visible=False)

    ax2.set_ylabel('p(%s|det)'%column)
    plt.setp(ax2.get_yticklabels(), visible=False)

    ax1.grid(True, which='both')
    ax2.grid(True, which='both')

    for ax in [ax1, ax2]:
        ax.tick_params(
            left=True,
            right=True,
            top=True,
            bottom=True,
            direction='in',
            which='both',
        )

    plt.subplots_adjust(
        left=0.08,
        right=0.98,
        bottom=0.08,
        top=0.98,
        hspace=0.03,
    )

    ### return
    return fig

#------------------------

def corner(events, columns, column_labels, log=[], label=None, fig=None):
    ndim = len(columns)

    ranges = []
    for col in columns:
        ranges.append(samples2range(events[col], log=col in log))

    # iterate and make plot
    if fig is None:
        fig = plt.figure(figsize=(1.5*ndim, 1.5*ndim))

    Ndet = len(events)

    # iterate through diagonals to find ranges
    for row in range(ndim):
        ax = fig.add_subplot(ndim, ndim, 1 + row*ndim + row)

        xmin, xmax = ranges[row]
        bins = np.linspace(xmin, xmax, max(10, int(0.5*Ndet**0.5)))
        dx = bins[1] - bins[0]

        n, b, p = ax.hist(
            events[columns[row]],
            bins=bins,
            histtype='step',
            density=True,
            label=label,
        )
        color = p[0].get_edgecolor()

        ### add error bars
        x, y, dy = errors(n*dx, b, Ndet)
        ax.fill_between(x, (y-dy)/dx, (y+dy)/dx, color=color, alpha=0.10)

        if row != ndim-1:
            plt.setp(ax.get_xticklabels(), visible=False)
        else:
            ax.set_xlabel(column_labels[columns[row]])
            plt.setp(ax.get_xticklabels(), rotation=90)

        plt.setp(ax.get_yticklabels(), visible=False)

        m, M = ax.get_xlim()
        ranges[row] = (min(m, xmin), max(M, xmax))

        ax.set_xlim(ranges[row])
#        ax.set_ylim(ymin=0.)

    # iterate again now that ranges have been updated
    for row in range(ndim):
        for col in range(row):
            ax = fig.add_subplot(ndim, ndim, 1 + row*ndim + col)

            ax.plot(
                events[columns[col]],
                events[columns[row]],
                color=color,
                alpha=max(0.05, 1./len(events)),
                marker='.',
                linestyle='none',
            )

            ax.set_xlim(ranges[col])
            ax.set_ylim(ranges[row])

            if row != ndim-1:
                plt.setp(ax.get_xticklabels(), visible=False)
            else:
                ax.set_xlabel(column_labels[columns[col]])
                plt.setp(ax.get_xticklabels(), rotation=90)

            if col != 0:
                plt.setp(ax.get_yticklabels(), visible=False)
            else:
                ax.set_ylabel(column_labels[columns[row]])

    # finish decorating
    for ax in fig.axes:
        ax.tick_params(
            left=True,
            right=True,
            top=True,
            bottom=True,
            direction='in',
            which='both',
        )

    plt.subplots_adjust(
        hspace=0.03,
        wspace=0.03,
        left=0.15,
        bottom=0.15,
        right=0.98,
        top=0.98,
    )

    ### return
    return fig, color
