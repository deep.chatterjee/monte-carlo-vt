#!/usr/bin/env python3

"""a one-off script to test mcvt-hdf2series
"""
__author__ = "Reed Essick (reed.essick@gmail.com)"

#-------------------------------------------------

import os

from collections import defaultdict

import h5py
import numpy as np

import matplotlib
matplotlib.use("Agg")
from matplotlib import pyplot as plt

from argparse import ArgumentParser

### non-standard libraries
from gwdistributions.backends import names
from gwdistributions.event import Event
from gwdistributions.transforms import TimeOfArrival

from gwdetectors import (parse, Detector)

from montecarlovt import io as mcvtio

#-------------------------------------------------

#COLORS = ['g', 'r', 'm', 'cornflowerblue', 'orange']
COLORS = plt.rcParams['axes.prop_cycle'].by_key()['color']

#-------------------------------------------------

parser = ArgumentParser()

parser.add_argument('tabular_data', type=str)
parser.add_argument('series_data', nargs='+', type=str)

parser.add_argument('network', type=str)

parser.add_argument('--channel-name', nargs=2, default=[], type=str, action='append',
    help='set the channel name. Can be repeated. e.g. "--channel-name LLO RNP04-LLO"')

parser.add_argument('--pre-window', type=float, default=1.0,
    help='the amount of time before coalescence that is included in each plot. Specified in seconds. \
DEFAULT=1.0')
parser.add_argument('--post-window', type=float, default=0.25,
    help='the amount of time after coalescence that is included in each plot. Specified in seconds. \
DEFAULT=0.25')

parser.add_argument('--name', default=[], nargs=2, type=str, action='append',
    help='update the naming convention used')

parser.add_argument('-o', '--output-dir', default='.', type=str)
parser.add_argument('-t', '--tag', default='', type=str)

parser.add_argument('-v', '--verbose', default=False, action='store_true')
parser.add_argument('-V', '--Verbose', default=False, action='store_true')

args = parser.parse_args()

os.makedirs(args.output_dir, exist_ok=True)

args.verbose |= args.Verbose

if args.tag:
    args.tag = "_" + args.tag

channames = dict((b, a) for a, b in args.channel_name)

#-------------------------------------------------

# update naming convention

for old, new in args.name:
    names.use(old, new, verbose=args.Verbose)

#-------------------------------------------------

# load network to compute time-of-arrivals
network = parse(args.network, verbose=args.verbose)
toa = TimeOfArrival(network=network)

#-------------------------------------------------

# load tabular data

if args.verbose:
    print('loading tabular data from: '+args.tabular_data)

with h5py.File(args.tabular_data, 'r') as obj:
    attrs = dict(obj.attrs.items())
    events = obj['events'][:]

num_events = len(events)

if args.verbose:
    print('    found %d events' % num_events)

#------------------------

# load series data

channels = defaultdict(list)

for path in args.series_data:
    start, dur = [int(_) for _ in path.split('.')[-2].split('-')[-2:]]
    if args.verbose:
        print('loading series data for (%d, %d) from: %s' % (start, start+dur, path))

    data, gps_start, dt = mcvtio.load_series(path, verbose=args.Verbose)
    num_samples = len(data)

    assert gps_start == start, 'mismatch between filename and gps_start reported within file'
    assert num_samples*dt == dur, 'mismatch between filename and duration computed from array length and dt'

    channels['time'].append( (start, start+dur, np.arange(start, start+dur, dt)) )
    for channel in data.dtype.names:
        channels[channel].append( (start, start+dur, data[channel]) )

# concatenate series

for channel, data in channels.items():
    data.sort(key=lambda x : x[0]) ### order these by their start time
    for ind in range(len(data)-1):
        assert data[ind][1] == data[ind+1][0], 'gap detected for channel=%s'%channel

    # append the data
    channels[channel] = np.concatenate(tuple(datum[2] for datum in data))

time = channels.pop('time') ### treat this as a special case

num_channels = len(channels)

#-------------------------------------------------

# now, iterate over tabular data and make a sanity-check plot for each

fig_series_tmp = os.path.join(args.output_dir, os.path.basename(__file__) + '-time-domain' + args.tag + '-%06d.png')
fig_angles_tmp = os.path.join(args.output_dir, os.path.basename(__file__) + '-orientation' + args.tag + '-%06d.png')

for ind, event in enumerate(events):
    if args.verbose:
        print('processing event %6d / %6d' % (ind, num_events))

    #--------------------

    # first, let's make a plot of the time-series data around the event

    if args.verbose:
        print('    plotting time-series data')

    fig = plt.figure(figsize=(6, 8))

    # figure out the expected time-of-arrival at each detector
    gps = event[names.name('geocenter_time')]

    ### compute time-of-arrival
    e = Event()
    e[names.name('geocenter_time')] = gps
    e[names.name('right_ascension')] = event[names.name('right_ascension')]
    e[names.name('declination')] = event[names.name('declination')]

    toa.transform(e)

    # grab the window we should plot
    sel = (gps - args.pre_window <= time) * (time <= gps + args.post_window) # select window of time

    for cnd, (channel, data) in enumerate(channels.items()):
        if args.Verbose:
            print('    plotting time-domain data around gps=%.6f sec for : %s' % (gps, channel))

        ax = plt.subplot(num_channels, 1, 1+cnd)

        color = COLORS[cnd%len(COLORS)]

        ax.plot((time[sel] - gps)*1e3, data[sel], alpha=0.75, color=color)

        # add an annotation for time at geocenter and expected time-of-arrival
        ylim = ax.get_ylim()

        if channel in channames:
            t0 = (e[toa._dt_name(channames[channel])] - gps) * 1e3

            ax.fill_between(
                [t0, 0],
                [ylim[0]]*2,
                [ylim[1]]*2,
                color='k',
                alpha=0.10,
                zorder=-10,
            )

            ax.plot([t0]*2, ylim, color='k', alpha=0.75, zorder=-10)

        ax.plot([0]*2, ylim, color='k', alpha=0.75, zorder=-10)

        ax.set_ylim(ylim)

        # set the x limits
        ax.set_xlim(xmin=-args.pre_window*1e3, xmax=+args.post_window*1e3)

        # legend
        ax.text(
            (-args.pre_window + (args.post_window + args.pre_window)*0.01) * 1e3,
            ylim[1] - (ylim[1]-ylim[0])*0.05,
            channel,
            color=color,
            ha='left',
            va='top',
        )

        # decorate

        ax.set_ylabel('$h$')

        if cnd < num_channels-1:
            plt.setp(ax.get_xticklabels(), visible=False)
        else:
            ax.set_xlabel('$t-t_\mathrm{geo}\,[\mathrm{ms}]$')

        # finish decorating
        ax.grid(True)
        ax.tick_params(
            left=True,
            right=True,
            top=True,
            bottom=True,
            direction='in',
            which='both',
        )

    # finish adjustsing
    plt.subplots_adjust(
        left=0.12,
        right=0.95,
        top=0.95,
        bottom=0.07,
        hspace=0.15,
    )

    # save
    figname = fig_series_tmp % ind
    if args.verbose:
        print('    saving : '+figname)
    fig.savefig(figname)
    plt.close(fig)

    #---------------------

    # now, make a plot of the detector locations relative to the direction to the source (RA, Dec)
    # this will let us sanity-check the correct sign of the time-of-flight delay

    if args.verbose:
        print('    plotting detector locations relative to line-of-sight to source')

    fig = plt.figure(figsize=(6, 6))
    ax = fig.add_axes([0.08, 0.08, 0.84, 0.84], projection='polar')

    # convert line-of-sight to the source into geographic coordinates
    phi, theta = Detector._geographic_angles(
        gps,
        event[names.name('right_ascension')],
        event[names.name('declination')],
        coord='celestial',
    )

    nZ = np.cos(theta)
    nP = np.sin(theta)
    nX = nP*np.cos(phi)
    nY = nP*np.sin(phi)

    # plot the detectors along with their time delays

    for dnd, detector in enumerate(network.detectors):
        dt = detector._geographic_dt(phi, theta)

        dX, dY, dZ = detector.location

        dP = (dX*nX + dY*nY)/nP

        radius = (dP**2 + dZ**2)**0.5
        pole = np.arctan2(dP, dZ)

        color = COLORS[dnd%len(COLORS)]

        ax.plot(pole, radius, marker='o', color=color, alpha=0.75)

        if np.abs(pole-theta) > np.pi/2:
            ax.plot([pole, theta+np.pi], [radius, -radius*np.cos(pole-theta)], color=color, alpha=0.50)
        else:
            ax.plot([pole, theta], [radius, radius*np.cos(pole-theta)], color=color, alpha=0.50)

        label = '$t_\mathrm{%s} - t_\mathrm{geo} = %s\,\mathrm{ms}$' % \
            (detector.name, ('%.1f' if dt < 0 else '+%.1f') % (dt*1e3))

        ax.text(
            pole,
            radius,
            label,
            ha='center',
            va='center',
        )

    # add annotation for the line-of-sight to the event
    lim = 0.030 ### a little bigger than the radius of the Earth in sec

    ax.plot([0, theta], [0, lim], color='k', marker='.')
    ax.text(theta, lim*1.05, 'to source', ha='center', va='center')

    ax.plot([0, theta+np.pi], [0, lim], color='k', marker='.', linestyle='dashed')
    ax.text(theta+np.pi, lim*1.05, 'away from source', ha='center', va='center')

    ax.set_rticks([]) # make these go away so we can see the other annotations

    ax.grid(True, which='both')

    ax.tick_params(
        left=True,
        right=True,
        top=True,
        bottom=True,
        direction='in',
        which='both',
    )

    # save
    figname = fig_angles_tmp % ind
    if args.verbose:
        print('    saving : '+figname)
    fig.savefig(figname)
    plt.close(fig)
