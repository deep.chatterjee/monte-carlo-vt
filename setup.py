#!/usr/bin/env python
__usage__ = "setup.py command [--options]"
__description__ = "standard install script"
__author__ = "Reed Essick (reed.essick@ligo.org)"

#-------------------------------------------------

from setuptools import (setup, find_packages)
import glob

setup(
    name = 'monte-carlo-vt',
    version = '0.0',
    url = 'https://git.ligo.org/reed.essick/monte-carlo-vt',
    author = __author__,
    author_email = 'reed.essick@ligo.org',
    description = __description__,
    license = 'MIT',
    scripts = glob.glob('bin/*'),
    packages = find_packages(),
    data_files = [],
    requires = ['numpy', 'scipy', 'matplotlib', 'h5py', 'gwdistributions', 'gwdetectors'],
    package_data = {},
)
